## Food Club
### Dependencies
- [Composer](https://getcomposer.org/)
- [Node.js](https://nodejs.org/en/)

### Getting started

- clone repo
```
git clone https://github.com/jclyons52/menu-maker
```
- install dependencies
```
cd menu-maker
composer install
npm install
```
- Create a database and run the migrations
```
php artisan migrate
```

- Serve the application
```
php artisan serve
```
- Run the build process
```
gulp watch
```
- Run just the tests
```
gulp tdd
```
or
```
vendor/bin/phpunit
```