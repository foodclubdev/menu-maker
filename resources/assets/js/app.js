
import Vue from "vue"
import VueResource from "vue-resource"
import MenuItems from "./views/menuItems/menuItems"


Vue.use(VueResource)

var App = new Vue({
    el: '#vue-app',

    ready: function() {
        console.log('hello world');
    },

    components: {
        'menu-items-view': MenuItems
    }
});