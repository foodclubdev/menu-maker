@extends('app')

@section('content')

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-sm-4">
                    <img class=" img product-thumbnail" src="{{route('files.show',$event->thumbnail_id )}}" alt="{{$event->name}}" height="300px" width="300px">
                </div>
                <div class="col-sm-8">
                    <div class="caption-full">
                        <h4 class="pull-right">
                            <div class="col-xs-12">
                                <h3>Attending: {{ $event->attendingCount() }}</h3>

                            </div>
                            <br/>
                            <br/>
                            <div class="col-xs-12">
                                @include('partials.attendingForm')
                            </div>
                        </h4>
                        <h1>{{$event->name}}</h1>
                        <p>{{$event->description}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <menu-items></menu-items>
    @if(Auth::check())
        @if(Auth::user()->isHost($event->id))
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Create new menu item (this panel is only visible if you are the host)
                    </div>
                    <div class="panel-body">
                        <form action="/menu-items" method="POST" enctype="multipart/form-data">
                            <input type="hidden" value="{{$event->id}}" name="event_id" />
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input class="form-control" name="name" type="text"/>
                            </div>
                            <div class="form-group">
                                <label for="description">Description:</label>
                                <input class="form-control" name="description" type="text"/>
                            </div>
                            <div class="form-group">
                                <label for="price">Price:</label>
                                <input class="form-control" name="price" type="text"/>
                            </div>
                            <div class="form-group">
                                <label for="file">Thumbnail:</label>
                                <input type="file" name="file"/>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-default"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        @endif
    @endif
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Menu Items
            </div>
            <div class="panel-body">



                @foreach($event->menuItems as $menuItem)

                    <div class="media">
                        <div class="media-left">

                            <img class=" img product-thumbnail" src="{{route('files.show',$menuItem->thumbnail_id )}}" alt="{{$menuItem->name}}" height="80" width="80">

                        </div>
                        <div class="media-body">
                            <div class="col-xs-4">
                                <h4 class="media-heading">{{$menuItem->name}}</h4>
                                <p>{{str_limit($menuItem->description, 70)}}</p>
                                <p>Price: {{$menuItem->price}}</p>
                            </div>

                                <div class="col-xs-6">
                                    @include('partials.orderForm')
                                </div>



                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </div>
    <br/><br/><br/><br/>
@endsection