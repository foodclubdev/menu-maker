<ul>
    @foreach($events as $event)
        <div class="media">
            <div class="media-left">
                <a href="{{route('events.show', $event->id)}}">
                    <img class=" img product-thumbnail" src="{{route('files.show',$event->thumbnail_id )}}" alt="{{$event->name}}" height="80" width="80">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">{{$event->name}}</h4>
                <p>{{str_limit($event->description, 200)}}</p>
            </div>
        </div>

    @endforeach
</ul>