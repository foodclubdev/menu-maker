@extends('app')

@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" href="#collapseSearch" aria-expanded="false" aria-controls="collapseExample">
                        Refine Search
                    </a>
                </div>
                <div class="panel-body collapse" id="collapseSearch">
                    <form class="form" action="/events/search">
                        <input type="hidden" name="lng" value="{{$location['lng']}}">
                        <input type="hidden" name="lat" value="{{$location['lat']}}">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Vegan
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Vegitarian
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Gluten free
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="points"> Distance </label>
                            <input type="text" id="textInput" name="radius" class="form-control" style="width: 20%" value="50">
                            <input type="range" name="points" value="50" min="0" max="50" onchange="updateTextInput(this.value);">
                        </div>
                        <div class="form-group">
                            <label for="from">From</label>
                            <input type="date" id="from" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="to">To</label>
                            <input type="date" id="to" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Results </div>

                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        @foreach($events as $i => $event)
                            <li data-i="{{$i}}" class="map-sidebar">
                                <a href="{{route('events.show', $event->id)}}">
                                    <div class="media">
                                        <div class="media-left">

                                            <img class=" img product-thumbnail" src="{{route('files.show',$event->thumbnail_id )}}" alt="{{$event->name}}" height="80" width="80">

                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">{{$event->name}}</h4>
                                            <p>{{str_limit($event->description, 70)}}</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <hr/>

                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div><select id="locationSelect" style="width:100%;visibility:hidden"></select></div>
            <div id="map-canvas"></div>
        </div>

    </div>
@endsection
@section('styles')
    <style>

        @media (max-width: 768px) {
            #map-canvas{
                width:35.5em;
                height:37.5em;
                margin: auto;
            }
        }
        @media (min-width: 768px) {
            #map-canvas{
                width:43.75em;
                height:37.5em;
                margin: auto;
            }

        }
    </style>

@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script type="text/javascript">
        function updateTextInput(val) {
            document.getElementById('textInput').value=val;
        }
    </script>
    <script id="info-window-template" type="text/template">
        <div id="content">
            <img class=" img product-thumbnail" src="{{url('/')}}/files/<%= event.thumbnail_id %>" alt="<%= event.name %>" height="150px" width="300px">
            <div id="siteNotice">
            </div>
            <h1 id="firstHeading" class="firstHeading"><%= event.name %></h1>
            <div id="bodyContent">
                <p> <%= event.description %> </p>
                <p>Location: <%= event.address %> </p>
                <p><a href="/events/<%= event.id %>">Go To</a></p>
            </div>
        </div>
    </script>
    <script>


        var map;
        var gmarkers = [];
        var userLat = {{$location['lat']}};
        var userLng = {{$location['lng']}};
        var events = {!!json_encode($events) !!};
        var infoWindows = [];


        google.maps.event.addDomListener(window, 'load', initialize);
        $('.map-sidebar').on('mouseenter', openInfoWindow);
        $('.map-sidebar').on('mouseleave', closeInfoWindow);


        function initialize() {
            map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 11,
                center: {lat: userLat, lng: userLng}
            });

            _.each(events, addMarker);
        }

        function addMarker(mrker) {
            var latLng = new google.maps.LatLng(mrker.lat, mrker.lng);
            // Creating a marker and putting it on the map
            var marker = new google.maps.Marker({
                position: latLng,
                title: mrker.name
            });
            var contentTemplate = _.template($('#info-window-template').html());
            var contentString = contentTemplate({event: mrker});

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            google.maps.event.addListener(marker, 'click', function() {
                openInfoWindow = infowindow.open(map,marker);
                console.log(openInfoWindow);
            });
            marker.setMap(map);
            gmarkers.push(marker);
            infoWindows.push(infowindow);
        }

        function openInfoWindow(e){
            var i = $(this).data('i');
            google.maps.event.trigger(gmarkers[i], "click");

        }

        function closeInfoWindow(){
            var i = $(this).data('i');
            infoWindows[i].close();
        }







    </script>
@endsection