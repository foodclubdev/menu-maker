@extends('app')

@section('content')
<div class="row">
    <h1 class="pull-left">Roles</h1>
    <a class="btn btn-primary pull-right" style="margin-top: 25px;" href="{!! route('admin.roles.create') !!}">Add New</a>
</div>
<div class="row">
    <table class="table">
        <thead>
        <th> Name</th>
        <th> Description</th>
        <th> actions</th>
        </thead>
        <tbody>
        @foreach($roles as $role)
            <tr>
                <td>
                    <a href="{!! route('admin.roles.show', [$role->id]) !!}">
                        {{$role->name}}
                    </a>
                </td>
                <td>
                    {{$role->description}}
                </td>
                <td>
                    <a href="{!! route('admin.roles.edit', [$role->id]) !!}" id="role-edit-{{$role->id}}"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('admin.roles.delete', [$role->id]) !!}" id="role-delete-{{$role->id}}"><i class="glyphicon glyphicon-remove"></i>Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>
</div>


@endsection