@extends('app')

@section('content')

    <form action="{{ route('admin.roles.store') }}" method="POST">
        <input type="hidden" value="{{csrf_token()}}" name="_token"/>
        @include('admin.roles.fields')
    </form>
@endsection