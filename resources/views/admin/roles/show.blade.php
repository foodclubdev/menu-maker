@extends('app')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <h1>{{$role->name}}</h1>
            <p>{{$role->description}}</p>
        </div>
    </div>
    @endsection