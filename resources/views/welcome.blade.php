<!DOCTYPE html>
<html>
    <head>
        <title>Food Club</title>
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
        <style>
            html, body {
                height: 100%;

            }
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
                background: url(/img/landing_page.jpg) no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .content {
                text-align: center;
                display: inline-block;
            }
            .title {
                font-size: 96px;
                color: black;
                -webkit-text-fill-color: white; /* Will override color (regardless of order) */
                -webkit-text-stroke-width: 1px;
                -webkit-text-stroke-color: black;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Food Club</div>
                <a class="btn btn-primary btn-lrg" href="#" disabled="disabled"><h2>Requesting location...</h2></a>
            </div>
        </div>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position){
                        $('.btn').attr('href','/events/search?lat='+position.coords.latitude+'&lng='+position.coords.longitude+'&radius=50');
                        $('.btn').attr('disabled', false);
                        $('.btn').html('<h3>Find a meal near you</h3>');
                    });
                }
            });
        </script>
    </body>
</html>
