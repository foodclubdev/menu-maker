@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">My Events </div>

				<div class="panel-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a role="tab" href="#attending" data-toggle="tab">Attending</a></li>
                        <li role="presentation"><a role="tab" href="#hosting" data-toggle="tab">Hosting</a></li>
                    </ul>
                    <br/><br/>
                    <div class="tab-content">
                        <div class="tab-pane active" id="attending">
                            @include('events.list', ['events' => Auth::user()->events()->wherePivot('type', 'attending')->get()])
                        </div>
                        <div class="tab-pane" id="hosting">
                            @include('events.list', ['events' => Auth::user()->events()->wherePivot('type', 'host')->get()])
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection