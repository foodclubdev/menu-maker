
@if(Auth::check())
    @if(Auth::user()->isHost($event->id))
        <a class="glyphicon glyphicon-remove btn btn-danger" href="{!! route('attending.delete', [$event->id]) !!}"> delete</a>
    @elseif(Auth::user()->isAttending($event->id))
        <a class="glyphicon glyphicon-log-out" href="{!! route('attending.delete', [$event->id]) !!}"> leave</a>
    @else
        <a class="glyphicon glyphicon-log-in" href="{!! route('attending.store', [$event->id]) !!}"> join</a>
    @endif
@else
    <a class="glyphicon glyphicon-log-in" href="{!! route('attending.store', [$event->id]) !!}"> join</a>
@endif