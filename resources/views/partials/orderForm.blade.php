@if(Auth::check())
    @if(Auth::user()->hasOrdered($menuItem->id))
        <a class="btn btn-danger glyphicon glyphicon-remove" id="menuItem-{{$menuItem->id}}" href="{!! route('orders.delete', [$menuItem->id]) !!}"> cancel</a>
    @else
        <a class="btn btn-success glyphicon glyphicon-plus" id="menuItem-{{$menuItem->id}}" href="{!! route('orders.store', [$menuItem->id]) !!}"> order</a>
    @endif
@else
    <a class="btn btn-success glyphicon glyphicon-plus" id="menuItem-{{$menuItem->id}}" href="{!! route('orders.store', [$menuItem->id]) !!}"> order</a>
@endif