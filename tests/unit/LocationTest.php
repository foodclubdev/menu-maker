<?php

use App\FoodClub\Exceptions\LatitudeOutOfRangeException;
use App\FoodClub\Exceptions\LongitudeOutOfRangeException;
use App\FoodClub\Location;

class LocationTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function it_gets_distance_between_locations()
    {
        $melb = ['lat' => -37.8141, 'lng' => 144.9633 ];

        $syd = ['lat' => -33.8675, 'lng' => 151.2070];

        $melbToSydDistance = 713800;

        $melbourne = new Location($melb['lat'], $melb['lng']);

        $sydney = new Location($syd['lat'], $syd['lng']);

        $dist = $melbourne->distanceFrom($sydney);

        $this->assertTrue($melbToSydDistance * 0.99 < $dist);
        $this->assertTrue($melbToSydDistance * 1.01 > $dist);
    }
    
    /**
     * @test
     */
    public function it_throws_an_error_if_lat_is_out_of_range()
    {
        $this->setExpectedException(LatitudeOutOfRangeException::class);
        new Location(-96.0, 108.0);
    }

    /**
     * @test
     */
    public function it_throws_an_error_if_lng_is_out_of_range()
    {
        $this->setExpectedException(LongitudeOutOfRangeException::class);
        new Location(37.0, 197.0);
    }
    
    /**
     * @test
     */
    public function it_converts_radius_in_meters_to_degrees()
    {
        $radius = 0.17940292336;

        $melb = ['lat' => -37.8141, 'lng' => 144.9633 ];

        $melbourne = new Location($melb['lat'], $melb['lng']);

        $angle = $melbourne->convertDistanceToCentralAngle(713800);

        $this->assertTrue($radius * 0.99 < $angle);
        $this->assertTrue($radius * 1.01 > $angle);
        
    }
}