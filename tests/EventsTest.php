<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventsTest extends TestCase
{
    use Illuminate\Foundation\Testing\DatabaseTransactions;

    public function test_search_for_location()
    {
        $event = factory('App\Event')->create();

        $this->visit("/events/search?lng={$event->lng}&lat={$event->lat}&radius=50")
            ->see($event->name);
    }

    public function test_show_event()
    {
        $event = factory('App\Event')->create();

        $this->visit('/events/'.$event->id)
            ->see($event->name);
    }

    public function test_create_event(){
        $user = factory('App\User')->create();

        $faker = Faker\Factory::create();
        $name = $faker->word;
        $this->actingAs($user)
            ->visit('/events/create')
            ->type($name, 'name')
            ->type($faker->sentence, 'description')
            ->type($faker->address, 'address')
            ->type($faker->latitude,'lat')
            ->type($faker->longitude, 'lng')
            ->attach(public_path('test_files/test.png'), 'file')
            ->press('submit')
            ->seeInDatabase('events', ['name' => $name]);

    }

    public function test_guest_cannot_create_event()
    {
        $this->visit('/events/create')
            ->seePageIs('/auth/login');
    }

    public function test_menu_items_shown()
    {

    }


}