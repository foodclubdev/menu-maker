<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderingTest extends TestCase
{
    use Illuminate\Foundation\Testing\DatabaseTransactions;


    public function test_attende_can_order()
    {
        $user = factory(App\User::class)->create();
        $event = factory(App\Event::class)->create();
        $menuItem = factory(App\MenuItem::class)->create([
            'event_id' => $event->id,
        ]);

        $user->events()->save($event, ['type' => 'attendng']);

        $this->actingAs($user)
            ->visit(route('events.show',$event->id))
            ->click('menuItem-'.$menuItem->id)
            ->seeInDatabase('menu_item_user', ['user_id' => $user->id, 'menu_item_id' => $menuItem->id]);

    }

    public function test_attende_can_cancel_order_before_served()
    {

    }

    public function test_attende_cannot_cancel_after_served()
    {

    }

    public function test_host_can_see_orders()
    {

    }

    public function test_host_can_accept_orders()
    {

    }

    public function test_host_can_charge_attende()
    {

    }

    public function test_user_can_see_invoice()
    {

    }
}