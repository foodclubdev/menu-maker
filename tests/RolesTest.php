<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RolesTest extends TestCase
{
    use Illuminate\Foundation\Testing\DatabaseTransactions;

    public function test_admin_filter_accepts_admin()
    {
        $role = factory('App\Role')->create([
            'name' => 'admin',
        ]);
        $user = factory('App\User')->create();
        $user->role()->associate($role);
        $user->save();

        $this->actingAs($user)
            ->visit('/admin/roles')
            
            ->seePageIs('/admin/roles');

    }

    public function test_admin_filter_redirects_non_admin()
    {
        $user = factory('App\User')->create();

        $this->actingAs($user)
            ->visit('/admin/roles')
            ->seePageIs('/home');

    }

    public function test_roles_index_page()
    {
        $role = factory(App\Role::class)->create();

        $this->withoutMiddleware()
            ->visit('/admin/roles')
            ->see($role->name);
    }
    public function test_show_role()
    {
        $role = factory('App\Role')->create();

        $this->withoutMiddleware()
            ->visit('admin/roles')
            ->click($role->name)
            ->seePageIs("admin/roles/{$role->id}");

    }

    public function test_edit_role()
    {
        $role = factory('App\Role')->create();

        $this->withoutMiddleware()
            ->visit('admin/roles')
            ->click('role-edit-'.$role->id)
            ->seePageIs("admin/roles/{$role->id}/edit");
    }

    public function test_roles_delete_link()
    {
        $role = factory('App\Role')->create();

        $this->withoutMiddleware()
            ->visit('/admin/roles')
            ->click('role-delete-'.$role->id)
            ->dontSee($role->name);

    }

    public function test_add_role()
    {
        $this->withoutMiddleware()
             ->visit('/admin/roles')
             ->click('Add New')
             ->seePageIs('/admin/roles/create')
            ->type('newRole', 'name')
            ->type('this is a new role', 'description')
            ->press('submit')
            ->seePageIs('/admin/roles')
            ->see('newRole');
    }
}