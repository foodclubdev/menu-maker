<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventUserTest extends TestCase
{
    use Illuminate\Foundation\Testing\DatabaseTransactions;

    public function test_guest_cannot_attend()
    {
        $event = factory('App\Event')->create();
        $this->visit('/events/'.$event->id)
            ->click('join')
            ->seePageIs('/auth/login');
    }

    public function test_host_cannot_attend()
    {
        $user = factory('App\User')->create();
        $event = factory('App\Event')->create();
        $user->events()->save($event, ['type' => 'host']);
        $this->visit('/events/'.$event->id);
    }

    public function test_redirect_login_joins_event()
    {
        $event = factory('App\Event')->create();
        $user = factory('App\User')->create([
            'password' => bcrypt('secret')
        ]);

        $this->visit('/events/'.$event->id)
            ->click('join')
            ->type($user->email, 'email')
            ->type('secret', 'password')
            ->press('Login')
            ->seePageIs('/events/'.$event->id);
    }

    public function test_user_can_join_and_leave_event()
    {
        $event = factory('App\Event')->create();
        $user = factory('App\User')->create();

        $this->actingAs($user)
            ->visit('/events/'.$event->id)
            ->click('join')
            ->see('leave')
            ->click('leave')
            ->see('join');
    }
}