<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MenuItemsTest extends TestCase
{
    use Illuminate\Foundation\Testing\DatabaseTransactions;

    public function test_view_all_for_event()
    {
        $event = factory(\App\Event::class)->create();
        $menuItems = factory(\App\MenuItem::class)->create([
            'event_id' => $event->id,
        ]);
        $this->visit('/events/'.$event->id)
            ->see($menuItems->name);
    }
    public function test_guest_cannot_create_menu_item()
    {

        $event = factory(\App\Event::class)->create();
        $menuItem = factory(\App\MenuItem::class)->make([
            'event_id' => $event->id,
        ]);

        $this->withoutMiddleware()
            ->visit('/events/'.$event->id)
            ->dontSee(' Create new menu item');

        $response = $this->call('POST', '/menu-items', $menuItem->toArray());

        assert($response->getStatusCode() == 404);

    }

    public function test_host_can_create_item()
    {
        $user = factory(\App\User::class)->create();
        $event = factory(\App\Event::class)->create();
        $user->events()->save($event, ['type' => 'host']);

        $faker = Faker\Factory::create();
        $name = $faker->sentence;

        $this->actingAs($user)
            ->visit('/events/'.$event->id)
            ->type($name, 'name')
            ->type($faker->paragraph, 'description')
            ->type($faker->randomDigit, 'price')
            ->attach(public_path('test_files/test.png'), 'file')
            ->press('submit')
            ->seeInDatabase('menu_items', ['name' => $name]);

    }
}