<?php

namespace App\FoodClub;

use App\FoodClub\Exceptions\LatitudeOutOfRangeException;
use App\FoodClub\Exceptions\LongitudeOutOfRangeException;

class Location
{
    public $lat;

    public $lng;

    public $state;

    public $city;

    public $suburb;

    public $postcode;

    public $address;

    const earthRadius = 6371000;

    const latMax = 90;

    const latMin = -90;

    const lngMax = 180;

    const lngMin = -180;

    public function __construct($lat, $lng)
    {
        if ($lat < self::latMin || $lat > self::latMax) {
            throw new LatitudeOutOfRangeException("lat must be between 90 and -90. Value provided was: {$lat}");
        }
        if ($lng < self::lngMin || $lng > self::lngMax) {
            throw new LongitudeOutOfRangeException("lng must be between 180 an -180. Value provided was: {$lng}");
        }

        $this->lat = $lat;

        $this->lng = $lng;
    }


    public function distanceFrom(Location $location)
    {
        $distance = self::haversineGreatCircleDistance(
            $this->lat,
            $this->lng,
            $location->lat,
            $location->lng
        );

        return $distance;
    }

    public function distanceToLatLngRange($distance)
    {
        $angleInDegrees = $this->convertDistanceToCentralAngle($distance);

        $range = [
            'lat' => [
                'max' => $this->lat + $angleInDegrees,
                'min' => $this->lat - $angleInDegrees
            ],
            'lng' => [
                'max' => $this->lng + $angleInDegrees,
                'min' => $this->lng - $angleInDegrees
            ]
        ];

        return $range;
    }

    static function  haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $centralAngle = self::centralAngle($latTo, $latFrom, $lonTo, $lonFrom);

        $distance = 2 * self::earthRadius * asin(sqrt($centralAngle));
        return round($distance);
    }

    /**
     * @param $distance
     * @return mixed
     */
    public function convertDistanceToCentralAngle($distance)
    {
        $angleInRadians = sin($distance / (2 * self::earthRadius)) ** 2;

        $angleInDegrees = rad2deg($angleInRadians);

        return $angleInDegrees;
    }

    /**
     * @param $latTo
     * @param $latFrom
     * @param $lonTo
     * @param $lonFrom
     * @return float
     */
    private static function centralAngle($latTo, $latFrom, $lonTo, $lonFrom)
    {
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $centralAngle = self::hav($latDelta) + cos($latFrom) * cos($latTo) * self::hav($lonDelta);

        return $centralAngle;
    }

    /**
     * @param $latDelta
     * @return float
     */
    private static function hav($theta)
    {
        return sin($theta / 2) ** 2;
    }
}