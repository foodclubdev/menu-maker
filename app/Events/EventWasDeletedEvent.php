<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Event as FoodEvent;

class EventWasDeletedEvent extends Event
{
    use SerializesModels;

    public $foodEvent;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(FoodEvent $event)
    {
        $this->foodEvent = $event;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
