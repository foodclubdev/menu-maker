<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EventsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {



    }

    public function search(Request $request)
    {
        $input = $request->all();
        $center_lat = $input["lat"];
        $center_lng = $input["lng"];
        $radius = $input["radius"];

        $events = DB::select("SELECT *, ( 3959 * acos( cos( radians('{$center_lat}') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('{$center_lng}') ) + sin( radians('{$center_lat}') ) * sin( radians( lat ) ) ) ) AS distance FROM events HAVING distance < '{$radius}' ORDER BY distance LIMIT 0 , 20");

        return view('events.search', ['events' => $events, 'location' => $input]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();

        $file = $input['file'];
        $file_id = $this->addThumbnail($file);
        $input['thumbnail_id'] = $file_id;

        $event = Event::create($input);

        $user->events()->save($event, ['type' => 'host']);

        return redirect(url('/home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('events.show', ['event' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
