<?php

namespace App\Http\Controllers;

use App\Event;
use App\MenuItem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MenuItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();

        if(array_key_exists('file', $input)){
            $file = $input['file'];
            $file_id = $this->addThumbnail($file);
            $input['thumbnail_id'] = $file_id;
        }


        $event = Event::findOrFail($input['event_id']);
        if(Auth::check() && $user->isHost($event->id)){
            $menuItem = MenuItem::create($input);
            $event->menuItems()->save($menuItem);

        }else{
            return response('you do not have permission to edit this event', 404);
        }
        return redirect(route('events.show', $event->id));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
