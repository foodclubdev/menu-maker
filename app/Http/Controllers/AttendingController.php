<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AttendingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store($eventId)
    {
        $user = Auth::user();
        if($user->isHost($eventId)){
            return redirect(route('events.show', $eventId));
        }
        $user->events()->attach($eventId, ['type' => 'attending']);


        return redirect(route('events.show', $eventId));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($eventId)
    {
        $user = Auth::user();
        if($user->isHost($eventId)){
            $event = Event::find($eventId);
            $user->events()->detach($eventId);
            $event->delete();
            return redirect(url('/home'));
        }else{
            $user->events()->detach($eventId);
            return redirect(route('events.show', $eventId));
        }



    }
}
