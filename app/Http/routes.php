<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('events/search', 'EventsController@search');

Route::resource('files', 'FileEntryController');

Route::resource('events', 'EventsController');

Route::resource('menu-items', 'MenuItemController');

Route::get('attend/{id}', [
    'as' => 'attending.store',
    'uses' => 'AttendingController@store'
]);

Route::get('unattend/{id}', [
    'as' => 'attending.delete',
    'uses' => 'AttendingController@destroy'
]);

Route::get('order/add/{id}', [
    'as' => 'orders.store',
    'uses' => 'OrdersController@store'
]);

Route::get('order/remove/{id}', [
    'as' => 'orders.delete',
    'uses' => 'OrdersController@destroy'
]);

Route::group(['middleware' => 'admin'], function(){
    Route::resource('/admin/roles', 'RolesController');
    Route::get('/admin/roles/{id}/delete', [
        'as' => 'admin.roles.delete',
        'uses' => 'RolesController@destroy'
    ]);
});


