<?php

namespace App\Listeners;

use App\Events\EventWasDeletedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventWasDeletedListened
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventWasDeletedEvent  $event
     * @return void
     */
    public function handle(EventWasDeletedEvent $event)
    {
        //
    }
}
