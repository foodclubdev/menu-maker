<?php

namespace App;

use App\FileEntry;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    public $fillable = ['name', 'description', 'address','lat', 'lng', 'thumbnail_id'];

    public static $rules =[
    ];

    public function thumbnail()
    {
        return $this->hasOne('\App\FileEntry', 'id' ,'thumbnail_id');
    }

    public function users()
    {
        return $this->belongsToMany('\App\User')->withPivot('type');
    }

    public function attendingCount()
    {
        return $this->users()->wherePivot('type','attending')->count();
    }

    public function menuItems()
    {
        return $this->hasMany('\App\MenuItem');
    }
}
