<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, BillableContract
{
    use Authenticatable, CanResetPassword, Billable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event')->withPivot('type');
    }

    public function isAttending($event_id)
    {
        $user_events = $this->events()->wherePivot('type', 'attending')->lists('id')->toArray();
        return in_array($event_id, $user_events);
    }

    public function isHost($event_id)
    {
        $event_list = $this->events()->wherePivot('type', 'host')->lists('id')->toArray();

        if (in_array($event_id, $event_list)){
            return true;
        }

        return false;
    }

    public function menuItems()
    {
        return $this->belongsToMany('App\MenuItem')->withPivot('status');
    }

    public function hasOrdered($menuItem_id)
    {
        $user_menuItems = $this->menuItems()->lists('id')->toArray();

        if(in_array($menuItem_id, $user_menuItems)){
            return true;
        }
        return false;
    }

}
