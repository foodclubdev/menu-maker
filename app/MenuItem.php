<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    public $fillable = ['name', 'description', 'thumbnail_id', 'price', 'event_id'];

    public function event()
    {
        return $this->belongsTo('\App\Event');
    }
}
