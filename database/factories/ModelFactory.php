<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\FileEntry::class, function ($faker){
    return [
        'filename' => $faker->word,
        'mime' => $faker->mimeType,
        'original_filename' => $faker->word,

    ];
});

$factory->define(App\Role::class, function ($faker){
    return [
        'name' => $faker->slug,
        'description' => $faker->sentence,
    ];
});

$factory->define(App\Event::class, function($faker){
    return [
        'name' => $faker->word,
        'description' => $faker->email,
        'address' => $faker->word,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
    ];
});

$factory->define(App\MenuItem::class, function($faker){
    return [
        'event_id' => $faker->randomDigit,
        'name' => $faker->word,
        'description' => $faker->sentence,
        'price' => $faker->randomDigit,
    ];
});